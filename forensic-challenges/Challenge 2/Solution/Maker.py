from midiutil import MIDIFile

# Adding a little bit of randomness to the notes
dic_notes = {
  0: 60,
  1: 40,
  2: 64,
  3: 67,
}

with open("flag.txt") as f:
  flag = f.read()

def CreateMIDI(flag):
  # create your MIDI object
  mf = MIDIFile(1)     # only 1 track
  track = 0   # the only track

  time = 0    # start at the beginning
  channel = 0
  mf.addTrackName(track, time, "Loud Flag")
  mf.addTempo(track, time, 120)

  for i, x in enumerate(flag):
    for j, bit in enumerate(x):
      if bit == '0':
        velocity = 0
      else:
        velocity = 100
    
      # add some notes
      pitch = dic_notes[j%4]
      time = (i * 8) + j
      duration = 1         # 1 beat long
      mf.addNote(track, channel, pitch, time, duration, velocity)

  # write it to disk
  with open("../Challenge/flag.mid", 'wb') as outf:
    mf.writeFile(outf)

# Convert the flag each character into binary
lt_bin = [bin(ord(c))[2:].zfill(8) for c in flag]

CreateMIDI(lt_bin)
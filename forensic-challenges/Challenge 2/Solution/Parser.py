from MIDI import MIDIFile

def parse(file):
    c=MIDIFile(file)
    c.parse()

    lt_bits = []
    for _, track in enumerate(c):
        track.parse()
        for event in track:
          if "velocity" in dir(event.message):
            if event.message.onOff == "OFF":
              continue
            
            if event.message.velocity == 100:
              lt_bits.append("1")
            else:
              lt_bits.append("0")
    
    lt_bits = "".join(lt_bits)
    if len(lt_bits) % 8 != 0:
      lt_bits = "0" + lt_bits

    # Convert the binary into ascii
    lt_ascii = []
    for i in range(0, len(lt_bits), 8):
      lt_ascii.append(chr(int(lt_bits[i:i+8], 2)))
    print("".join(lt_ascii))



parse("../Challenge/output.mid")
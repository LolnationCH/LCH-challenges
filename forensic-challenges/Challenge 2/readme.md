# Challenge 2

A nice midi jam, who doesn't like that? But with a track name like that, there's something hidden in this file for sure.

## Important Note

This challenge can be done by hand, but making a program might be faster.

# LCH's Challenges

## What is this

This is a series of challenge to test your wits against. This allow to learn new stuff and expand you horizon.

Multiple categories are planned to be added.

For now, here's a list of the challenges :

- Cryptography-Challenges
  - Theses challenges involves some basic cryptography stuff, encoding secret message so in case they fall in the wrong hands, they can't be read. Obviously you will want to read them!
    - Challenge 1 (Completed)
    - Challenge 2 (Completed)
    - Challenge 3 (Completed)
    - Challenge 4 (Completed)
    - Extra Challenge (In Progress)
- Esoteric-Challenges
  - Theses challenges have either weird quirks, uses esoteric languages or requires a "thinking-out-of-the-box" to be resolved.
    - Challenge 1 (Completed)
    - Challenge 2 (Completed)
- Forensic-Challenges
  - Theses challenges involve different file structure, manipulation, etc...
    - Challenge 1 (Completed)
    - Challenge 2 (Completed)

## Flags

Flags are always using this structure : `lch{XXXXXXXXXXXXXXX}` where `X` are random alphanumerical values, with various length.

Case don't matters, both lower and upper are accepted.

# Challenge 2

Guten Morgen, I have another cipher for you. It's from a german enigma m3.

Here's the rotor positions :
Rotor 1 (VI)  A, ring A
Rotor 2 (I)   Q, ring A
Rotor 3 (III) L, ring A

But it seems to have lost it's plugboard. Time to crack it.

Seems like the people using it didn't have `{`/`}` characters, so you can just ignore them.

Good luck !

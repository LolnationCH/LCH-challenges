import random

lt_char = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
lt_ind = [x for x in range(0, len(lt_char))]
length = len(lt_char)
sub_dict = {}

# Substitution dictionary
for i in range(0, len(lt_char)):
  n_pos = random.randrange(0, len(lt_ind))
  sub_dict[lt_char[i]] = lt_char[lt_ind.pop(n_pos)]

with open("flag.txt", "r") as f:
  flag = f.read().lower()
  
# Substitute each letter in the flag with the corresponding letter in the substitution dictionary
for i in range(0, len(flag)):
  if flag[i] in sub_dict:
    flag = flag[:i] + sub_dict[flag[i]] + flag[i + 1:]

# save the flag to a file
with open("../Challenge/secret_message.txt", "w") as f:
  f.write(flag)
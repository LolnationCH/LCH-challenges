import math

with open("flag.txt", "r") as f:
    flag = f.read()

def mod_ex(b, k, m):
    i = 1
    j = 0
    while (j <= k):
        b = (b * i) % m
        i = b
        j += 1
    return b

def PowMod(b, e, m):
    bin_e = bin(e)
    bin_e = bin_e[::-1]
    ln = len(bin_e)
    result = 1
    for i in range(0, ln - 2, +1):
        if (bin_e[i] == '1'):
            result *= mod_ex(b, i, m)
    return result % m

def Encrypt(message, modulo, e):
    cytxt = ""
    temp = modulo // 256
    per_char = 0
    while (temp != 0):
        temp = temp // 256
        per_char += 1
    cyln = pow(256, per_char)
    cyln = len(str(cyln)) + 1
    s = message
    for i in range(0, len(message), +per_char):
        s1 = s[:per_char]
        b = ConvertToInt(s1)
        cytxt1 = str(PowMod(b, e, modulo))
        if (len(cytxt1) < cyln):
            while ((cyln - len(cytxt1) > 0)):
                cytxt1 = "0" + cytxt1
            cytxt += cytxt1
        else:
            cytxt += cytxt1
        s = s[per_char:]
    to_i = int(cytxt)
    to_h = '%X' % to_i
    hex_cytxt = str(to_h)
    return hex_cytxt

def ConvertToInt(message):
    grd = 1
    num = 0
    message = message [::-1]
    for i in range(0,len(message),+1):
        num = num+ord(message[i])*grd
        grd *= 256
    return num

# Flag to int
e=65537
p=230863
q=286541
cipher = Encrypt(flag, p*q, e)

with open("../Challenge/flag.txt", "w") as f:
  f.write(f"e:{e}\n")
  f.write(f"N:{p*q}\n")
  f.write(f"cipher:{cipher}\n")
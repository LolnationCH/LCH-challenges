import math

with open("../Challenge/flag.txt", "r") as f:
  content = f.readlines()

e = int(content[0].split(":")[1])
N = int(content[1].split(":")[1])
cipher = content[2].split(":")[1]

# Algorithm to get the prime factors
# We can just brute force this since the N is low enough

def prime_factors(n):
  i = 2
  factors = []
  while i * i <= n:
    if n % i:
        i += 1
    else:
        n //= i
        factors.append(i)
  if n > 1:
    factors.append(n)
  return factors

p,q = prime_factors(N)

def eea(a, b):
    if (a % b == 0):
        return (b, 0, 1)
    else:
        gcd, s, t = eea(b, a % b)
        s = s - ((a // b) * t)
        return (gcd, t, s)

def InvertModulo(e, phi):
    gcd, s, _ = eea(e, phi)
    if (gcd != 1):
        return None
    else:
        return s % phi

def ConvertToStr(num):
    st = ""
    while (num != 0):
        temp = num % 256
        st += chr(temp)
        chr(temp)
        num = num - temp
        num = num // 256
    st = st[::-1]
    return st

def mod_ex(b, k, m):
    i = 1
    j = 0
    while (j <= k):
        b = (b * i) % m
        i = b
        j += 1
    return b

def PowMod(b, e, m):
    bin_e = bin(e)
    bin_e = bin_e[::-1]
    ln = len(bin_e)
    result = 1
    for i in range(0, ln - 2, +1):
        if (bin_e[i] == '1'):
            result *= mod_ex(b, i, m)
    return result % m

def Decrypt(ciphertext,modulo, e, p, q):
    to_i = int(ciphertext, 16)
    ciphertext = str(to_i)
    phi = (p - 1) * (q - 1)
    d = InvertModulo(e, phi)
    temp = modulo // 256
    per_char = 0
    while (temp != 0):
        temp = temp // 256
        per_char += 1
    cyln = pow(256, per_char)
    cyln = len(str(cyln)) + 1
    while (len(ciphertext) % cyln != 0):
        ciphertext = "0" + ciphertext
    s = ciphertext
    m = ""
    for i in range(0, len(ciphertext), +cyln):
        s1 = s[:cyln]
        b = int(s1)
        m += ConvertToStr(PowMod(b, d, modulo))
        s = s[cyln:]
    return m

mess = Decrypt(cipher, N, e, p, q)
print(mess)

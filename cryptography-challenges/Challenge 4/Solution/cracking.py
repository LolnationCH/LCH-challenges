import gmpy2
from functools import reduce 

with open("../Challenge/flag.txt", "r") as f:
  content = f.readlines()

e = int(content[0].split(":")[1])
n = int(content[1].split(":")[1])
cipher = content[2].split(":")[1]

def ConvertToStr(num):
    st = ""
    while (num != 0):
        temp = num % 256
        st += chr(temp)
        chr(temp)
        num = num - temp
        num = num // 256
    st = st[::-1]
    return st

# https://www.alpertron.com.ar/ECM.HTM
r =[1017847360883, 558152724851, 1055668007281, 1006434544981, 806516961421, 1054396673537, 822014892833, 893773263511, 1004924722391, 558144116623, 574837023307, 760440547957, 753905436793, 771629130343, 953747205007, 822281216651, 900137164739, 1086419369363, 602547034487, 589995591421, 1045485985393, 745849276901, 656349727057, 634676207447, 613671251257, 574070776523, 959765396537, 730578537883, 953506091029, 673528459001, 1082814870031, 707346334633, 789480162407, 824845827997, 956143968859, 965729536313, 824032765669, 877155303961, 929762073077, 1013591639681]
assert(n == reduce(lambda x, y: x * y, r))

phi_n = 1
for i in range(len(r)):
    phi_n *= (r[i] - 1)

d = gmpy2.invert(e, phi_n)

cipherNum = int(cipher, 16)
plaintext = pow(cipherNum, d, n)
plaintext = ConvertToStr(plaintext)

print(plaintext)

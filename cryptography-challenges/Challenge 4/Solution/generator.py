import random
from functools import reduce 

with open("flag.txt", "r") as f:
    flag = f.read()

def ConvertToInt(message):
    grd = 1
    num = 0
    message = message [::-1]
    for i in range(0,len(message),+1):
        num = num+ord(message[i])*grd
        grd *= 256
    return num

def GenerateRandomPrime(lenght):
    set_primes = []
    for i in range(0, lenght):
        while True:
            num = random.randint(2**(lenght-1), 2**(lenght))
            if is_prime(num) and num not in set_primes:
                set_primes.insert(i, num)
                break
    return set_primes

# Check if number is prime
def is_prime(n):
    if n == 2:
        return True
    if n % 2 == 0 or n < 2:
        return False
    for i in range(3, int(n**0.5)+1, 2):
        if n % i == 0:
            return False
    return True

# r = GenerateRandomPrime(40)
r =[1017847360883, 558152724851, 1055668007281, 1006434544981, 806516961421, 1054396673537, 822014892833, 893773263511, 1004924722391, 558144116623, 574837023307, 760440547957, 753905436793, 771629130343, 953747205007, 822281216651, 900137164739, 1086419369363, 602547034487, 589995591421, 1045485985393, 745849276901, 656349727057, 634676207447, 613671251257, 574070776523, 959765396537, 730578537883, 953506091029, 673528459001, 1082814870031, 707346334633, 789480162407, 824845827997, 956143968859, 965729536313, 824032765669, 877155303961, 929762073077, 1013591639681]
for x in r:
    assert(is_prime(x))

# Calculate N
N = reduce(lambda x, y: x * y, r)
e = 65537
cipher = ""

def mod_ex(b, k, m):
    i = 1
    j = 0
    while (j <= k):
        b = (b * i) % m
        i = b
        j += 1
    return b

def PowMod(b, e, m):
    bin_e = bin(e)
    bin_e = bin_e[::-1]
    ln = len(bin_e)
    result = 1
    for i in range(0, ln - 2, +1):
        if (bin_e[i] == '1'):
            result *= mod_ex(b, i, m)
    return result % m

def Encrypt(message, modulo, e):
    cytxt = ""
    temp = modulo // 256
    per_char = 0
    while (temp != 0):
        temp = temp // 256
        per_char += 1
    cyln = pow(256, per_char)
    cyln = len(str(cyln)) + 1
    s = message
    for i in range(0, len(message), +per_char):
        s1 = s[:per_char]
        b = ConvertToInt(s1)
        cytxt1 = str(PowMod(b, e, modulo))
        if (len(cytxt1) < cyln):
            while ((cyln - len(cytxt1) > 0)):
                cytxt1 = "0" + cytxt1
            cytxt += cytxt1
        else:
            cytxt += cytxt1
        s = s[per_char:]
    to_i = int(cytxt)
    to_h = '%X' % to_i
    hex_cytxt = str(to_h)
    return hex_cytxt

cipher = Encrypt(flag, N, e)

with open("../Challenge/flag.txt", "w") as f:
  f.write(f"e:{e}\n")
  f.write(f"N:{N}\n")
  f.write(f"cipher:{cipher}\n")
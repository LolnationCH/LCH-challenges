with open("../Challenge/secret_message.txt", "r") as f:
  flag = f.read()

# We know that the flag has the fourth letter has '{'
# So we can find the shift in the alphabet
lt_char = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '}']
length = len(lt_char)
cesar_key = None

inde = lt_char.index('{')
c = lt_char.index(flag[3])

cesar_key = (c - inde) % length

with open("../Challenge/secret_message.txt", "r") as f:
  flag = f.read()

for x in flag:
  if x in lt_char:
    print(lt_char[(lt_char.index(x) - cesar_key) % length], end="")
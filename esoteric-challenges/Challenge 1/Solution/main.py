import os

def GetFoldersInPath(path):
    return [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]

def GetFolders(path):
  res = []
  first_level = GetFoldersInPath(path)
  
  lt_folder_to_check = []
  for x in first_level:
    res.append(x)
    second_level = GetFolders(path + "\\" + x)
    if len(second_level) > 0:
      lt_folder_to_check.append(path + "\\" + x)

  for x in lt_folder_to_check:
    res.extend(GetFolders(x))
    
  return res


print("".join(["l"] + GetFolders("../Challenge/l")))
# Challenge 1

This folder structure seems to be hiding a secret. Can you find the flag?

## Important Note

The filesystem dictates how to represent the folders in what order, assume that you must sort by alphabetical order and `{`/`}` are always first.

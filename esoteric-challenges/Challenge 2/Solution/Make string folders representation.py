import os

with open("flag.txt", "r", encoding="ASCII") as f:
    flag = f.read()

bin_lt = []
for x in flag:
  bin_rep = bin(ord(x))[2:]
  bin_lt.append(bin_rep.zfill(8))


output_path = "output"
for i in range(0, len(bin_lt)):
  cur_char = bin_lt[i]
  second_folder = False
  cur_fold = f"{output_path}/New Folder ({i})"
  os.makedirs(cur_fold)

  first_folder_path = f"{cur_fold}/New Folder"
  second_folder_path = f"{cur_fold}/New Folder (2)"
  for p in range(0, len(cur_char)):
    if p >= 4:
      second_folder = True
    
    n_path = f"{second_folder_path if second_folder else first_folder_path}/New Folder ({p})"
    os.makedirs(n_path)
    if cur_char[p] == "1":
      os.makedirs(f"{n_path}/New Folder")